// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract Arrays {
    //Variable public quiere decir que cualquiera puede acceder a la información
    uint64 public number = type(uint64).max;

    uint8[5] public numeros;

    function setData() public {
        numeros[0] = 21;
        numeros[1] = 14;
    }

    //view: no va a escribir en la cadena pero sí va a consultar variables de estado
    //memory: para todo lo que son arreglos, y ya que debe guardar algo para retornarnolo
    function getData() public view returns(uint8[5] memory) {
        return numeros;
    }

    //MAPPINGS
    //Estructura clave valor
    mapping (string => address ) public accounts;
    accounts["Camilo"] = 0x15923jasdnxs92123828312;
}