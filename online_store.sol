//SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

contract OnlineStore{

    string public mensaje;

    function buySomething() external payable{
        require(msg.value == 0.001 ether); //require es como el if pero se cancela la ejecución en caso de que no cumpla
        mensaje = "vendido";
    }
}