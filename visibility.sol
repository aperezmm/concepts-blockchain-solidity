//SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

//Los contratos deberían estar separado por archivos
contract Base {
    uint public numero1 = 2;
    uint private numero2 = 4;
    uint internal numero3 = 6;

    function suma() private view returns(uint){
        return numero1 + numero2;
    }

    function llamarSuma() public view returns(uint){
        return suma();
    }

    function functionInternal() internal pure returns(string memory){
        return "Llamado a funcion interna";
    }

    function llamadoFunctionInternal() public pure returns(string memory){
        return functionInternal();
    }
}

contract Hijo is Base{

    function mostrarInternalVariable() public view returns(uint){
        return numero3;
    }

}