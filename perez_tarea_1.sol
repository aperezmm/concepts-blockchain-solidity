// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

contract Stackies {
    uint public x = 2;

    modifier checkEven(uint y){
        if(y % 2 == 0){
            _;
        }
    }

    function multiply(uint y) public checkEven(y){
        x = x * y;
    }

}